CC=gcc
CFLAGS=-Wall -std=c99 -g
LDFLAGS=-g
TARGET=serveur
LINK= -L${PWD}

all: $(TARGET) libmessage.so client moreless_cli moreless_serv test_cli test_serv

$(TARGET): $(TARGET).o libmessage.so
	$(CC) $(LINK) $(LDFLAGS) $< -lmessage -o $@

client: client.o client.h serveur.h
	$(CC) $(LINK) $(LDFLAGS) $< -lmessage -o $@

moreless_cli: moreless_cli.o libmessage.so moreless_cli.h moreless.h
	$(CC) $(LINK) $(LDFLAGS) $< -lmessage -o $@

moreless_serv: moreless_serv.o libmessage.so moreless_serv.h moreless.h
	$(CC) $(LINK) $(LDFLAGS) $< -lmessage -o $@

test_cli: test_cli.o libmessage.so
	$(CC) $(LINK) $(LDFLAGS) $< -lmessage -o $@ 

test_serv: test_serv.o libmessage.so
	$(CC) $(LINK) $(LDFLAGS) $< -lmessage -o $@

%.o: %.c %.h moreless.h serveur.h
	$(CC)  $(CFLAGS) -c -o $@ $<

libmessage.so: message.c message.h
	$(CC) -fPIC -c -o message.o message.c
	$(CC) -shared message.o -o libmessage.so

clean:
	rm -f *.o
	rm -f *.txt

mrproper: clean
	rm -f $(TARGET) libmessage.so client moreless_cli moreless_serv
	rm -f test_cli test_serv
