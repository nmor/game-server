# Project Server

   The *Server Project* is a program to run a server and a client. The server allows the creation and management of the game. The client allows interaction with the user.

## What is Project Server ?

   First, we created a dynamic library (libmessage.so). This library contains useful functions for tube communication. The libraries were tested using the test_cli.c and test_serv.c program on Moodle.

   With the valgrind command we could not detect memory leaks. At the same time we checked the implementation of the signals.

   In a second step, we implemented the "Smallest / Biggest" game. The biggest difficulty was to implement a "communication protocol", we wanted to create a generic and easily modifiable program. All communications have been checked (checking the argument passages), as well as the writing to the moreless_results file.

   After that, we managed the end of the wires. To do this, we used the SIGCHLD signal to kill the wires and display a trace in the console. We also managed the SIGINT, SIGQUIT and SIGTERM signals to ensure that the server stops properly. The biggest difficulty was fixing the bugs.

   Finally, the project was completed entierly. Nevertheless, some improvements are still possible

## Improvement

   The server is currently only a "console". In order for it to really become a server we must turn it into a daemon. To do this we need to redirect the standard error output to a log file, then launch the server as a daemon. For that we would have implemented a"./launch__daemon" program. Some parts of the code can be perceived as the writing of the nickname.

## Work separation

   The project was separated into subquestions we could easily separate the work. One of us took care of the testing and verification of the arguments as well as the implementation of the dynamic library. The other was responsible for the implementation of the signals as well as the server/client programs.
