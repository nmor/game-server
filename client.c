#include "client.h"
#include "serveur.h"

/**
 * Initalization of signals.
 */
volatile sig_atomic_t usr1_receive = 0;
volatile sig_atomic_t usr2_receive = 0;


int main(int argc, char *argv[]){


/* ----------------------------------------------------------------------------- *
 * -------------------       Check Arguments Validity        ------------------- *
 * ----------------------------------------------------------------------------- */

	if (argc < 2){
		fprintf(stderr, "Error: Invalid number of argument.\n");
		fprintf(stderr, "Usage : ./client <game> [args]\n");
		exit(1);
	}
    char *program = calloc(strlen(argv[1])+5, sizeof(char));
    if (program == NULL){
        perror("calloc");
        exit(6);
    }
    strcat(program, argv[1]);
    strcat(program, "_cli");

    /* Check if <game>_cli is runnable */
	int fd = access(program, X_OK);
	if (fd == -1){
		fprintf(stderr, "Error: Invalid program doesn't exist or not runnable\n");
		exit(2);
    }
    free(program);

    /* Read pid server */
    pid_t serveur = read_pid();
    if (serveur == -1){
        fprintf(stderr, "Error: Can't read PID of server, please check if server are running\n");
        exit(3);
    }

    /* Open named tube */
    char test[10];
    char *end[argc+2];
    sprintf(test, "%d", getpid());
    end[0] = test;
    end[1] = argv[1];

    for (int i = 2; i < argc; ++i){
        end[i] = argv[i];
    }
    end[argc] = (NULL);
    if (kill(serveur, SIGUSR1) == -1){
        perror("kill");
        exit(7);
    }
    fd = open(SERVER_PIPE, O_WRONLY);
    if (fd == -1){
        perror("open");
        exit(4);
    }
    send_argv(fd,end);
    if (close(fd) == -1){
        perror("close");
        exit(8);
    }

/* ----------------------------------------------------------------------------- *
 * -------------------       Installation of handlers         ------------------- *
 * ----------------------------------------------------------------------------- */

    struct sigaction action;
    sigemptyset(&action.sa_mask);
    action.sa_handler = handSIGUSR1;
    action.sa_flags = 0;
    sigaction(SIGUSR1, &action, NULL);

    /* Masking of the signal */
    sigset_t ens1;
    sigemptyset(&ens1);
    sigaddset(&ens1, SIGUSR1);
    sigprocmask(SIG_SETMASK, &ens1, NULL);
    sigset_t mask;
    sigemptyset(&mask);

    //Wait for signal USR1
    sigsuspend(&mask);

    if (usr1_receive){
        usr1_receive = 0;
        char path[]         = PATH_FIFO;
        char name[]         = NAME_FIFO;
        char endName0[]     = IN_FIFO;
        char endName1[]     = OUT_FIFO;
        char end0[BUF_SIZE] = "";

        strcat(end0 ,path);
        strcat(end0 ,name);
        strcat(end0, test);
        strcat(end0 ,endName0);

        int test_var = 0;
        test_var = open(end0, O_WRONLY);
        if (test_var == -1){
            perror("open");
            exit(4);
        }
        test_var = 0;

        char end1[BUF_SIZE] = "";

        strcat(end1 ,path);
        strcat(end1 ,name);
        strcat(end1, test);
        strcat(end1 ,endName1);

        test_var = open(end1, O_RDONLY);
        if (test_var == -1){
            perror("open");
            exit(4);
        }
        test_var = 0;

        char program[BUF_SIZE] = "";
        strcat(program, "./");
        strcat(program, argv[1]);
        strcat(program,"_cli");

        char **args = calloc(argc+1, sizeof(char *));
        if (args == NULL){
            perror("calloc");
            exit(6);
        }
        for (int i = 0; i < argc; ++i){
            args[i] = calloc(1, sizeof(char));
            if (args[i] == NULL){
                perror("calloc");
                exit(6);
            }
            args[i] = argv[i+1];
        }
        args[argc] = (NULL);

        //Unmask signal for children
        sigprocmask(SIG_SETMASK, &mask, NULL);

        execvp(program, args);
        perror("execvp");
        exit(5);
    }
    if (usr2_receive){
        printf("Error: Error from server\n");
        exit(2);
    }
}

/* ----------------------------------------------------------------------------- *
 * --------------------       Function of client        ------------------------ *
 * ----------------------------------------------------------------------------- */


/**
 * @brief Read PID in file SERVER_PID
 *
 * @return pid_t pid of the server
 */
pid_t read_pid(){
    char buf[10]; //Max for size_t is *******
    pid_t pid=-1;
    FILE *fp = fopen(SERVER_PID, "r");
    if (fp == NULL){
        return -1;
    }
    while (fgets(buf, sizeof(buf),fp) != NULL){
    }
    pid = atoi(buf);
    fclose(fp);
    return pid;
}

/**
 * @brief Activate when SIGUSR1 come.
 *
 * @param sig id of signal.
 */
void handSIGUSR1(int sig){
    usr1_receive = 1;
}

/**
 * @brief Activate when SIGUSR2 come.
 *
 * @param sig id of signal.
 */
void handSIGUSR2(int sig){
    usr2_receive = 1;
}
