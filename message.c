#include "message.h"


/**
 * @brief Size of tab
 *
 * @param str String to use
 * @return int size of tab
 */
int size_tab(char *str[]){
    int i = 0,length = 0;
    while (str[i] != NULL){
        length++;
        ++i;
    }
    return length;
}

/**
 * @brief Send a string to fd
 *
 * @param fd Tube for send.
 * @param str String to send.
 * @return int 0 if success.
 */
int send_string(int fd, char *str){
    size_t length = strlen(str);
    int wr[] = {length};
    if (write(fd, wr, 1) == -1){
        perror("write");
        return 1;
    }
    if (write(fd, str, length) == -1){
        perror("write");
        exit(1);
    }
    return 0;
}

/**
 * @brief Get a string in fd.
 *
 * @param fd     Tube for get.
 * @return char* String get.
 */
char *recv_string(int fd){
    int rd[] = {0};
    if (read(fd,rd, 1) == -1){
        perror("read");
        exit(1);
    }
    char *buf = calloc(rd[0]+1, sizeof(char));
    if (read(fd,buf,rd[0]) == -1){
        perror("read");
        exit(1);
    }
    return buf;
}

/**
 * @brief Send array of char
 *
 * @param fd    Tube for send.
 * @param argv  Array char to send.
 * @return int  0 if success.
 */
int send_argv(int fd, char *argv[]){
    int length = size_tab(argv);
    int wr[] = {length};

    if(write(fd,wr,1) == -1){
        perror("write");
        exit(1);
    }

    for (int i = 0; i < length; ++i){
        send_string(fd,argv[i]);
    }
    return 0;
}

/**
 * @brief Get array of char
 *
 * @param fd        Tube to get
 * @return char**   Array char
 */
char **recv_argv(int fd){
    int rd[] = {0};
    if (read(fd,rd,1) == -1){
        perror("read");
        exit(1);
    }

   char **buf = calloc(rd[0]+1,sizeof(char *));
   for (int i = 0; i < rd[0]; ++i){
       buf[i] = recv_string(fd);
   }
   buf[rd[0]] = NULL;
   return buf;
}
