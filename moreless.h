#ifndef MORELESS_H
#define MORELESS_H

#include <signal.h>

#define FILE_RESULT "/tmp/game_serveur/moreless_results"
#define BUF_SIZE 256
#define TIME 5
#define GREAT "great"
#define SMALL "small"
#define WON   "won"
#define FAIL  "fail"
#define MIN_PSEUDO_LENGTH 4

#endif
