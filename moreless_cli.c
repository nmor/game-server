#include "moreless_cli.h"
#include "moreless.h"

int main(int argc, char *argv[]){

    //Initialization of default value.
    int n = -1;  //Max attempts (default infinity)
    int m = 100; //Number between [1, M] (default 100)


/* ----------------------------------------------------------------------------- *
 * -------------------       Check Arguments Validity        ------------------- *
 * ----------------------------------------------------------------------------- */

    //Valid number of argc ?
    if (argc%2-1!=0 || argc > 5){
        if(argc != 1){
            use();
            return 1;
        }
    }

    //Only one parameters.
    if (argc < 4){
        if (argc != 1){
            if (strcmp(argv[1], "-m") == 0 || strcmp(argv[1], "-M") == 0){
                m = atoi(argv[2]);
            }
            else if (strcmp(argv[1], "-n") == 0 || strcmp(argv[1], "-N") == 0){
                n = atoi(argv[2]);
            }
            else {
                use();
                return 1;
            }
        }
    }

    //Two parameters.
    if(argc > 4){
        if (strcmp(argv[1], "-m") == 0 || strcmp(argv[1], "-M") == 0){
            m = atoi(argv[2]);
            if (strcmp(argv[3], "-n") == 0 || strcmp(argv[3], "-N") == 0){
                n = atoi(argv[4]);
            }
            else {
                use();
                return 1;
            }
        }
        else if (strcmp(argv[1], "-n") == 0 || strcmp(argv[1], "-N") == 0){
            n = atoi(argv[2]);
            if (strcmp(argv[3], "-m") == 0 || strcmp(argv[3], "-M") == 0){
                m = atoi(argv[4]);
            }
            else {
                use();
                return 1;
            }
        }
        else {
            use();
            return 1;
        }
    }

    if (n < 1 && n != -1){
        fprintf(stderr, "Error: invalid -n\n");
        exit(2);
    }

    if (m < 2){
        fprintf(stderr, "Error: invalid -m\n");
        exit(2);
    }

/* ----------------------------------------------------------------------------- *
 * -------------------       Installation of handlers         ------------------- *
 * ----------------------------------------------------------------------------- */

    struct sigaction action;
    sigemptyset(&action.sa_mask);
    action.sa_handler = handSIGUSR1;
    action.sa_flags = 0;
    sigaction(SIGUSR1, &action, NULL);

    pid_t pid = getpid();
    //Exchange pid with server.
    write(SERV_OUT_FILENO, &pid, sizeof(pid_t));
    read(SERV_IN_FILENO, &pid, sizeof(pid_t));


/* ----------------------------------------------------------------------------- *
 * ---------------------       Interact with user         ---------------------- *
 * ----------------------------------------------------------------------------- */

    printf("Welcome, you have to guess a number between %d and %d\n", 1, m);
    printf("You are entiled to %d attempts to find the number\n\n", n);
    printf("With each request, you have %d seconds to answer\n", TIME);
    printf("Begin of the game. Good luck !!!\n\n");


    //Initialization of the game.
    int turn = 1;
    int number = 0;
    char *buf;

    //User choose a number.
    while(1){
        printf("Choice %d, enter your choice : ", turn);
        char choice_number[m];
        fgets(choice_number, m, stdin);
        //If USR1 --> Server wait to long.
        if (usr1_receive == 1){
            printf("\n\nYou are out of time\n");
            printf("Bye.\n");
            return 0;
        }

        //Convert input number (string) to int.
        number = atoi(choice_number);
        if (write(SERV_OUT_FILENO, &number, sizeof(int)) == -1){
            perror("write");
            return 1;
        }
        buf = recv_string(SERV_IN_FILENO);

        //If value is >
        if (strcmp(buf, GREAT) == 0){
            printf("the number sought is greater\n\n");
        }

        //If value is <
        else if (strcmp(buf, SMALL) == 0){
            printf("the number sought is smaller\n\n");
        }

        //If value is =
        else if (strcmp(buf, WON) == 0){
            printf("You won, you found the number in %d attempts\n\n", turn);

            //Save result or don't save ?
            printf("Do you want save your result ? [Y/n] ");
            char choice[1];
            fread(&choice, sizeof(char), 2, stdin);
            if (ferror(stdin) == -1){
                perror("fread");
                return 2;
            }
            while(1){
                if (choice[0] == 'Y'){
                    break;
                }
                if (choice[0] == 'n'){
                    break;
                }
                printf("Error : %s\n", choice);
                printf("Do you want save your result ? [Y/n] ");
                fread(&choice, sizeof(char), 2, stdin);
                if (ferror(stdin) == -1){
                    perror("fread");
                    return 2;
                }
            }

            //Don't save
            if (choice[0] == 'n'){
                kill(pid, SIGUSR1);
                printf("You have chosen to not save your result\n");
                printf("Thank for your game good bye\n");
                return 0;
            }
            //Save
            printf("You have chosen to save your result,\n");
            printf("you must choose a pseudo with no space and minimum length %d\n\n", MIN_PSEUDO_LENGTH);

            //User enter pseudo
            printf("Enter your pseudo : ");
            char pseudo[BUF_SIZE];
            fgets(pseudo, BUF_SIZE, stdin);

            //Pseudo send to server
            send_string(SERV_OUT_FILENO, pseudo);
            int valid = -1;

            //Read server answer
            if (read(SERV_IN_FILENO, &valid, sizeof(int)) == -1){
                perror("read");
                return 2;
            }

            //If valid equal 1 pseudo contains error
            while (valid == 1){
                printf("Enter your pseudo : ");
                fgets(pseudo, BUF_SIZE, stdin);
                send_string(SERV_OUT_FILENO, pseudo);
                if (read(SERV_IN_FILENO, &valid, sizeof(int)) == -1){
                    perror("read");
                    return 2;
                }
            }

            //Exit program
            printf("Bye, save the result was succesful\n");
            return 0;
        }

        //User doesn't found the number.
        else if (strcmp(buf, FAIL) == 0){
            //Get the number
            if (read(SERV_IN_FILENO, &number, sizeof(int)) == -1){
                perror("read");
                return 2;
            }
            printf("You lost, the number sougth was %d\n\n", number);
            break;
        }
        turn++;
    }
}//End of program.

/* ----------------------------------------------------------------------------- *
 * -----------------       Function of moreless_cli         -------------------- *
 * ----------------------------------------------------------------------------- */

/**
 * @brief Action when signal USR1 receive.
 *
 * @param sig Signal receive.
 */
void handSIGUSR1(int sig){
    usr1_receive = 1;
}

/**
 * @brief Use when program launch with incorect paramaters.
 */
void use(){
    fprintf(stderr, "Usage: ./client %s [-m M] [-n N]\n", "moreless");
}
