#ifndef MORELESS_CLI_H
#define MORELESS_CLI_H

#define _POSIX_SOURCE
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include "message.h"

#define SERV_IN_FILENO 4
#define SERV_OUT_FILENO 3

volatile sig_atomic_t usr1_receive = 0;

void handSIGUSR1(int sig);
void use();

#endif
