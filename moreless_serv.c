#include "moreless_serv.h"
#include "moreless.h"

pid_t pid;

int main(int argc, char *argv[]){

    //Initialization of default value.

    int n = -1;  //Max attempts (default infinity)
    int m = 100; //Number between [1, M] (default 100)

    //Get seed from current time (more random)
    srand(time(NULL));

/* ----------------------------------------------------------------------------- *
 * -------------------       Check Arguments Validity        ------------------- *
 * ----------------------------------------------------------------------------- */

    //Valid number of argc ?
    if (argc%2-1!=0 || argc > 5){
        if(argc != 1){
            use();
            return 1;
        }
    }

    //Only one parameters.
    if (argc < 4){
        if (argc != 1){
            if (strcmp(argv[1], "-m") == 0 || strcmp(argv[1], "-M") == 0){
                m = atoi(argv[2]);
            }
            else if (strcmp(argv[1], "-n") == 0 || strcmp(argv[1], "-N") == 0){
                n = atoi(argv[2]);
            }
            else {
                use();
                return 1;
            }
        }
    }

    //Two parameters.
    if(argc > 4){
        if (strcmp(argv[1], "-m") == 0 || strcmp(argv[1], "-M") == 0){
            m = atoi(argv[2]);
            if (strcmp(argv[3], "-n") == 0 || strcmp(argv[3], "-N") == 0){
                n = atoi(argv[4]);
            }
            else {
                use();
                return 1;
            }
        }
        else if (strcmp(argv[1], "-n") == 0 || strcmp(argv[1], "-N") == 0){
            n = atoi(argv[2]);
            if (strcmp(argv[3], "-m") == 0 || strcmp(argv[3], "-M") == 0){
                m = atoi(argv[4]);
            }
            else {
                use();
                return 1;
            }
        }
        else {
            use();
            return 1;
        }
    }

    if (n < 1 && n != -1){
        fprintf(stderr, "Error: invalid -n\n");
        return 2;
    }

    if (m < 2){
        fprintf(stderr, "Error: invalid -m\n");
        return 2;
    }

/* ----------------------------------------------------------------------------- *
 * -------------------       Instalation of handlers         ------------------- *
 * ----------------------------------------------------------------------------- */

    struct sigaction action;
    sigemptyset(&action.sa_mask);
    action.sa_handler = sigAlarm;
    action.sa_flags = 0;
    sigaction(SIGALRM, &action, NULL);

    action.sa_handler = handSIGUSR1;
    sigaction(SIGUSR1, &action, NULL);

    //Get the secret number.
    int val = (int)(rand() / (double)RAND_MAX * (m - 1));

    int test = 1;

    int r = 0;

    //Exchange PID
    pid = getpid();

    if (write(1, &pid, sizeof(pid_t)) == -1){
        perror("write");
        return 3;
    }
    if (read(0, &pid, sizeof(pid_t)) == -1){
        perror("read");
        return 4;
    }

/*
* ---------------------------------------------------------------------
* -------------------      Interact with user      --------------------
* ---------------------------------------------------------------------
*/

    while (test != n+1){
        //User only have TIME to answer
        alarm(TIME);
        if (read(0, &r, sizeof(int)) == -1){
            if (kill(pid, SIGUSR1) == -1){
                perror("kill");
                return 5;
            }
            perror("read");
            return 4;
        }
        //Reset alarm
        alarm(0);
        //If SIGALRM have been send
        if (sigalarm == 1){
            //End of the game
            if (kill(pid, SIGUSR1) == -1){
                perror("kill");
                return 5;
            }
            return 0;
        }
        //If max number of attemps
        if (test == n){
            if (r != val){
                send_string(1, FAIL);
                if(write(1, &val, sizeof(int)) == -1){
                    perror("write");
                    return 3;
                }
                return 0;
            }
            else {
                break;
            }
        }
        //Secret number found
        if (r == val){
            break;
        }
        //Secret number are <
        else if (r > val){
            send_string(1, SMALL);
        }
        //Secret number are >
        else if (r < val) {
            send_string(1, GREAT);
        }
        test++;
    }

    //Player doesn't found secret number
    if (n != -1 && test > n){
        send_string(1,FAIL);
        if(write(1, &val, sizeof(int)) == -1){
            perror("write");
            return 3;
        }
    }
    //Player Win
    else {
        send_string(1, WON);
        char *user_pseudo;
        user_pseudo = recv_string(0);
        //If USR1 receive player doesn't want to save her result
        if (usr1_receive == 1){
            return 0;
        }

        //Check if pseudo is valid
        int valid = valid_pseudo(user_pseudo);
        while(valid == 1){
            if (write(1, &valid, sizeof(int)) == 1){
                perror("write");
                return 3;
            }
            user_pseudo = recv_string(0);
            valid = valid_pseudo(user_pseudo);
        }
        if (write(1, &valid, sizeof(int)) == -1){
            perror("write");
            return 3;
        }

        //Open file and save the result
        FILE *file = fopen(FILE_RESULT, "a");

        //Get time
        time_t rawtime = time(NULL);
        struct tm *ptm = localtime(&rawtime);
        char buf[BUF_SIZE] = {0};
        strftime(buf, BUF_SIZE, "%F at %T", ptm);

        //Write to the file
        fprintf(file, "%d;%d;%d;", test, m, val);
        int i = 0;
        while(user_pseudo[i] != '\n'){
            fprintf(file, "%c", user_pseudo[i]);
            ++i;
        }
        fprintf(file, ";%s\n", buf);
    }
    return 0;

}//End of program

/* ----------------------------------------------------------------------------- *
 * -----------------       Function of moreless_serv        -------------------- *
 * ----------------------------------------------------------------------------- */


/**
 * @brief Use when program launch with incorect paramaters.
 */
void use(){
    fprintf(stderr, "Usage: ./client %s [-m M] [-n N]\n", "moreless");
}

/**
 * @brief       Check if pseudo is valid (min LENGTH and contains no space).
 *
 * @param c     Pseudo to check
 * @return int  1 return0 if error 0 if not
 */
int valid_pseudo(char *c){
    int i = 0;
    while(strcmp(&c[i], "\n") != 0){
        if (c[i] == ' '){
            return 1;
        }
        ++i;
    }
    if (i < MIN_PSEUDO_LENGTH){
        return 1;
    }
    return 0;
}

/**
 * @brief Action when signal ALRM receive.
 *
 * @param sig Signal receive.
 */
void sigAlarm(int sig){
        sigalarm = 1;
}

/**
 * @brief Action when signal USR1 receive.
 *
 * @param sig Signal receive.
 */
void handSIGUSR1(int sig){
    usr1_receive = 1;
}
