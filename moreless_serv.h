#ifndef MORELESS_SERV_H
#define MORELESS_SERV_H

#define _POSIX_SOURCE
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include "message.h"

volatile sig_atomic_t usr1_receive = 0;
volatile sig_atomic_t sigalarm = 0;

void use();
int valid_pseudo(char *c);
void sigAlarm(int sig);
void handSIGUSR1(int sig);

#endif
