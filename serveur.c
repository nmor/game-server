/* Set Handler */
#include "serveur.h"

volatile sig_atomic_t usr1_receive = 0;
volatile sig_atomic_t child_receive = 0;
volatile sig_atomic_t term_receive = 0;

int main(int argc, char *argv[]){

    //Umask set to 0
    umask(0);

/* ----------------------------------------------------------------------------- *
 * ---------------------       Check Server Validity        -------------------- *
 * ----------------------------------------------------------------------------- */

        FILE* fp = NULL;
        fp = fopen(SERVER_PID, "r+");

        //File already exist
        if (fp != NULL){
            int test_var = fclose(fp);
            if (test_var == -1){
                perror("fclose");
                exit(1);
            }
            printf("Error: %s Already exist.\n", SERVER_PID);
            exit(0);
        }
        //File doesn't exist
        fp = fopen(SERVER_PID, "w");
        if (fp == NULL){
            perror("fopen");
            exit(2);
        }
        //Write pid on file
        pid_t pid = getpid();
        fprintf(fp, "%d\n", pid);
        int test_var = fclose(fp);
        if(test_var == -1){
            perror("fclose");
            exit(1);
        }

        fp = NULL;
        int test = access(SERVER_PIPE, R_OK | W_OK | X_OK);
        if (test == -1){
            printf("Tube doesn't exist\n");
            //Create named tube
            if(mkfifo(SERVER_PIPE, 0777) == -1){
                perror("mkfifo");
                exit(5);
            }
            if(chmod(SERVER_PIPE, 0777) == -1){
                perror("chmod");
                exit(11);
            }
        }
        //Named tube already exist.
        else {
            if (close(test) == -1){
                perror("close");
                exit(1);
            }
        }

        DIR *fd = NULL;
        fd = opendir(PATH_FIFO);
        //Directory doesn't exist
        if (fd == NULL){
            if (mkdir(PATH_FIFO, 0770) == -1){
                perror("mkdir");
                exit(2);
            }
        }
        //Directory exist
        else {
            if (closedir(fd) == -1){
                perror("closedir");
                exit(3);
            }
        }


/* ----------------------------------------------------------------------------- *
 * -------------------       Installation of handlers         ------------------- *
 * ----------------------------------------------------------------------------- */

    struct sigaction action;
    sigemptyset(&action.sa_mask);
    action.sa_handler = handSIGUSR1;
    action.sa_flags = 0;
    sigaction(SIGUSR1, &action, NULL);

    action.sa_handler =  handCHLD;
    sigaction(SIGCHLD, &action, NULL);

    action.sa_handler =  handEND;
    sigaction(SIGINT, &action, NULL);
    sigaction(SIGQUIT, &action, NULL);
    sigaction(SIGTERM, &action, NULL);


/* ----------------------------------------------------------------------------- *
 * -----------------------------       SERVER        --------------------------- *
 * ----------------------------------------------------------------------------- */

    int foa = -1;
    while(1){
        char path[]     = PATH_FIFO;
        char name[]     = NAME_FIFO;
        char endName0[] = IN_FIFO;
        char endName1[] = OUT_FIFO;
        foa = -1;
        pause();
        //Need to kill the child.
        if (child_receive){

            int status;
            wait(&status);
            fprintf(stderr, "**********************************\n");
            fprintf(stderr, "Child Information : \n");
            fprintf(stderr, "\tWIFEXITED   : %d\n", WIFEXITED(status));
            fprintf(stderr, "\tWEXITSTATUS : %d\n", WEXITSTATUS(status));
            fprintf(stderr, "\tWIFSIGNALED : %d\n", WIFSIGNALED(status));
            fprintf(stderr, "\tWTERMSIG    : %d\n", WTERMSIG(status));
            fprintf(stderr, "**********************************\n");
            child_receive = 0;
        }

        //New client want to play
        if (usr1_receive){
            foa = open(SERVER_PIPE, O_RDONLY);
            if (foa == -1){
                perror("open");
                exit(3);
            }
            char **recv = recv_argv(foa);
            if (close(foa) == -1){
                perror("close");
                exit(1);
            }
            int fo = -1;

            size_t size_for_tab = strlen(path) + strlen(name) + strlen(endName0) + strlen(recv[0]) + 1;

            char *end0 = calloc(size_for_tab, sizeof(char));
            if (end0 == NULL){
                perror("calloc");
                exit(4);
            }

            strcat(end0 ,path);
            strcat(end0 ,name);
            strcat(end0, recv[0]);
            strcat(end0 ,endName0);
            if (mkfifo(end0, 0777) == -1){
                perror("mkfifo");
                exit(5);
            }

            char *end1 = calloc(size_for_tab, sizeof(char));
            if (end1 == NULL){
                perror("calloc");
                exit(4);
            }

            strcat(end1 ,path);
            strcat(end1 ,name);
            strcat(end1, recv[0]);
            strcat(end1 ,endName1);

            if (mkfifo(end1, 0777) == -1){
                perror("mkfifo");
                exit(5);
            }
            pid_t pid_client = atoi(recv[0]);

            if (kill(pid_client, SIGUSR1) == -1){
                perror("kill");
                exit(6);
            }
            int foo = open(end0, O_RDONLY);
            if (foo == -1){
                    perror("open");
                    if (kill(pid_client, SIGUSR2) == -1){
                        perror("kill");
                        exit(6);
                    }
                    exit(2);
            }

            fo = open(end1, O_WRONLY);
            if (fo == -1){
                perror("open");
                if (kill(pid_client, SIGUSR2) == -1){
                    perror("kill");
                    exit(6);
                }
                exit(2);
            }

            pid_t fils = fork();
            //Child
            if (fils == 0){
                if (dup2(foo,0) == -1){
                    perror("dup2");
                    if (kill(pid_client, SIGUSR2) == -1){
                        perror("kill");
                        exit(6);
                    }
                    exit(7);
                }
                if (dup2(fo,1) == -1){
                    perror("dup2");
                    if (kill(pid_client, SIGUSR2) == -1){
                        perror("kill");
                        exit(6);
                    }
                    exit(7);
                }

                size_for_tab = strlen("./") + strlen(recv[1]) + strlen("_serv") + 1;
                char *program = calloc(size_for_tab, sizeof(char));
                if (program == NULL){
                    perror("calloc");
                    if (kill(pid_client, SIGUSR2) == -1){
                        perror("kill");
                        exit(6);
                    }
                    exit(4);
                }
                strcat(program, "./");
                strcat(program, recv[1]);
                strcat(program,"_serv");

                size_for_tab = strlen("./") + strlen(recv[1]) + strlen("_serv") + 1;
                char *programs = calloc(size_for_tab, sizeof(char));
                if (programs == NULL){
                    perror("calloc");
                    if (kill(pid_client, SIGUSR2) == -1){
                        perror("kill");
                        exit(6);
                    }
                    exit(5);
                }
                strcat(programs, recv[1]);
                strcat(programs, "_serv");
                free(recv[1]);
                recv[1] = programs;
                free(end0);
                free(end1);

                execvp(program, &recv[1]);
                perror("execvp");
                exit(8);
            }
            if (fils == -1){
                if (kill(pid_client, SIGUSR2) == -1){
                    perror("kill");
                    exit(6);
                }
                exit(9);
            }

            int length = size_tab(recv);
            for (int i = 0; i < length; ++i){
                free(recv[i]);
            }
            free(end0);
            free(end1);
            free(recv);
            usr1_receive = 0;
        }

        //Someone want to kill the server.
        //  Kill all child.
        //  Free all. (not need because come after USR1).
        //  Close the server.
        if (term_receive){
            int test_var = remove(SERVER_PID);
            if(test_var == -1){
                perror("remove");
                exit(10);
            }
            while(wait(NULL) != -1){
            }
            fprintf(stderr, "End of server\n");
            exit(0);
        }
    }
}

/* ----------------------------------------------------------------------------- *
 * ---------------------       Function of server         ---------------------- *
 * ----------------------------------------------------------------------------- */

/**
 * @brief Action when signal CHLD receive.
 *
 * @param sig Signal receive.
 */
void handCHLD(int sig){
    child_receive = 1;
}

/**
 * @brief Action when signal USR1 receive.
 *
 * @param sig Signal receive.
 */
void handSIGUSR1(int sig){
    usr1_receive = 1;
}

/**
 * @brief Action when signal TERM or INT or QUIT receive.
 *
 * @param sig Signal receive.
 */
void handEND(int sig){
    term_receive = 1;
}
