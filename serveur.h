#ifndef SERVEUR_H
#define SERVEUR_H

#define _POSIX_SOURCE

#include <dirent.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "message.h"  //Don't forget to execute `export LD_LIBRARY_PATH=$PWD` and the current terminal and on the current folder.
#include "serveur.h"

#define SERVER_PID  "/tmp/game_serveur.pid"
#define SERVER_PIPE "/tmp/game_serveur.fifo"

#define PATH_FIFO   "/tmp/game_serveur/"
#define NAME_FIFO   "cli"
#define IN_FIFO     "_0.fifo"
#define OUT_FIFO    "_1.fifo"

#define BUF_SIZE 256

#define MAX_ARG_STRLEN 2097152

void handCHLD(int sig);
void handSIGUSR1(int sig);
void handEND(int sig);



#endif
